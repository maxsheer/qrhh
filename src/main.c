#include "matrix.h"
#include <sys/time.h>
#include "cblas.h"
#include "cblas_f77.h"
#include "lapacke.h"
#include "lapack.h"


int                 sign(double x)
{
    if (x >= 0)
        return 1;
    else
        return -1;
}


void                put(double *mat, int cols, int rows)
{
    for (int i = 0; i < rows; ++i)
    {
        for (int j = 0; j < cols; ++j)
            printf("%.6f ", *(mat + j + i*cols));
        printf("\n");
    }
}
//эта функция реализует неблочный метод хаусхолдера. табличка делится на 4 части потому что так немного быстрее




void             householder_lapack(double** q, double **mat)
{  
    double      *v, *work;
    double      tau;


    //Consider initial matrix Mat as 4 blocks (b00, b01, b10, b11)
    //blocks initialization

    v = (double*)malloc(sizeof(double) * G_ROWS);
    work = (double*)malloc(sizeof(double) * G_ROWS);

    
    //initialization of utils
    *q = (double*)malloc(sizeof(double) * G_COLS * G_ROWS); //
    
    //делаем два цикла: по левой половине матрицы и по правой. Так избавляемся от рутины с правым верхним блоком + 
    // объекты становятся легче и охотнее лезут в кэш
    for (int i = 0; i < G_ROWS - 1; ++i)
    {
        
        //задаем вектор v. он состоит из i-го столбца левого верхнего блока b00 и левого нижнего блока v01
        for (int j = 0; j < i; ++j)
            *(v + j) = 0;
        
        for (int j = i; j < G_ROWS; ++j)
            *(v + j) = *(*mat + i + j * G_COLS);

        LAPACKE_dlarfg(G_COLS - i, (double*)(v + i), (double*)(v + i + 1), 1, &tau);
        for (int k = 0; k < i; ++k)
            (*q)[i + k * G_COLS] =  0;

        (*q)[i + i * G_COLS] =  1;
        *(v + i) = 1;


        for (int k = 0; k < G_COLS; ++k)
            (*q)[i + k * G_COLS] =  v[k];
        
        LAPACKE_dlarfx(LAPACK_ROW_MAJOR, 'L', G_COLS, G_ROWS, v, tau, *mat, G_COLS, work);        
    }

    return;
}





void             householder_blas(double** q, double **mat)
{  
    double      x2;
    double      x1;
    double*     b00;
    double*     b01;
    double*     b10;
    double*     b11;
    double*     v00;
    double*     v10;
    double*     w00;
    double*     w10;


    //Consider initial matrix Mat as 4 blocks (b00, b01, b10, b11)
    //blocks initialization
    b00 = (double*)malloc(sizeof(double) * BS * BS);
    b01 = (double*)malloc(sizeof(double) * BS * BS);
    b10 = (double*)malloc(sizeof(double) * BS * BS);
    b11 = (double*)malloc(sizeof(double) * BS * BS);
    for (int i = 0; i < BS; ++i)
    {
        for (int j = 0; j < BS; ++j)
        {
            b00[j + i * BS] = *(*mat + j + i * G_COLS);
            b01[j + i * BS] = *(*mat + BS + j + i * G_COLS);
            b10[j + i * BS] = *(*mat + j + (i + BS) * G_COLS);
            b11[j + i * BS] = *(*mat + BS + j + (i + BS) * G_COLS);
        }
    }
    
    //initialization of utils
    *q = (double*)malloc(sizeof(double) * G_COLS * G_ROWS); //
    v00 = (double*)malloc(sizeof(double) * BS);
    v10 = (double*)malloc(sizeof(double) * BS);
    w00 = (double*)malloc(sizeof(double) * BS);
    w10 = (double*)malloc(sizeof(double) * BS);
    
    //делаем два цикла: по левой половине матрицы и по правой. Так избавляемся от рутины с правым верхним блоком + 
    // объекты становятся легче и охотнее лезут в кэш
    for (int i = 0; i < BS; ++i)
    {
        //задаем вектор v. он состоит из i-го столбца левого верхнего блока b00 и левого нижнего блока v01
        for (int j = 0; j < i; ++j)
            *(v00 + j) = 0;
        
        for (int j = i; j < BS; ++j)
            *(v00 + j) = b00[i + j * BS];

        for (int j = 0; j < BS; ++j)
            *(v10 + j) = b10[i + j * BS];
        

        //норма v до смещения 
        x2 = cblas_ddot(BS, v00, 1, v00, 1) + cblas_ddot(BS, v10, 1, v10, 1);
        x2 = sqrt(x2);

        //смещение i-й компоненты v
        v00[i] += (double)sign(v00[i]) * x2;

        //сохраняем v в матрицу Q
        for (int k = 0; k < BS; ++k)
            (*q)[i + k * G_COLS] =  v00[k];   
        for (int k = 0; k < BS; ++k)
            (*q)[i + (k + BS) * G_COLS] =  v10[k];          

        //считаем скалярное пр-е v*v. потом на него будем делить
        x1 = cblas_ddot(BS, v00, 1, v00, 1) + cblas_ddot(BS, v10, 1, v10, 1);
        //w00

        //считаем векторы поправки: w = A^T v. Также делим w напополам, поэтому ниже два цикла, по одному на каждую половинку
        cblas_dgemv(CblasRowMajor, CblasTrans, BS, BS, 1, b00, BS, v00, 1, 0, w00, 1);
        cblas_dgemv(CblasRowMajor, CblasTrans, BS, BS, 1, b10, BS, v10, 1, 1, w00, 1);
        cblas_dgemv(CblasRowMajor, CblasTrans, BS, BS, 1, b01, BS, v00, 1, 0, w10, 1);
        cblas_dgemv(CblasRowMajor, CblasTrans, BS, BS, 1, b11, BS, v10, 1, 1, w10, 1);

        cblas_dger(CblasRowMajor, BS, BS, -(2 / x1), v00, 1, w00, 1, b00, BS);
        cblas_dger(CblasRowMajor, BS, BS, -(2 / x1), v00, 1, w10, 1, b01, BS);
        cblas_dger(CblasRowMajor, BS, BS, -(2 / x1), v10, 1, w00, 1, b10, BS);
        cblas_dger(CblasRowMajor, BS, BS, -(2 / x1), v10, 1, w10, 1, b11, BS);
        //вычитаем одноранговую поправку из основной матрицы
    }

    //для преобразования второй половинки требуется меньше действий. вместо четырех блоков b.. используем только b11,
    //ведь верхняя половина вектора v  занулится и не будет делать вклад в поправку. по этой причине также используем только
    //одну половину вектора v и одну половину вектора w. в целом все как наверху

    for (int i = 0; i < BS - 1; ++i)
    {
        //set x
        for (int j = 0; j < i; ++j)
            *(v00 + j) = 0;
        
        for (int j = i; j < BS; ++j)
            *(v00 + j) = b11[i + j * BS];
        
        x2 = cblas_ddot(BS, v00, 1, v00, 1);
        x2 = sqrt(x2);
        //set v
        v00[i] += (double)sign(v00[i]) * x2;

        for (int k = 0; k < BS; ++k)
        {
            *(*q + BS + i + k * G_COLS) = 0;

        }
        for (int k = 0; k < BS; ++k)
        {
            *(*q + BS + i + (k + BS) * G_COLS) = v00[k];   
        }

        x1 = cblas_ddot(BS, v00, 1, v00, 1);
        //w00
        cblas_dgemv(CblasRowMajor, CblasTrans, BS, BS, 1, b11, BS, v00, 1, 0, w00, 1);
        cblas_dger(CblasRowMajor, BS, BS, -(2 / x1), v00, 1, w00, 1, b11, BS);
    }

    //осталось сохранить результат в mat (берем и переносим 4 блока b назад в mat)
    for (int i = 0; i < BS; ++i)
        for (int j = 0; j < BS; ++j)
        {
            *(*mat + j + i * G_COLS) = b00[j + i * BS];
            *(*mat + BS + j + i * G_COLS) = b01[j + i * BS];
            *(*mat + j + (i + BS) * G_COLS) = b10[j + i * BS];
            *(*mat + j + BS + (i + BS) * G_COLS) = b11[j + i * BS];

        }
    return;
}



void             householder(double** q, double **mat)
{  
    double      x2;
    double      x1;
    double*     b00;
    double*     b01;
    double*     b10;
    double*     b11;
    double*     v00;
    double*     v10;
    double*     w00;
    double*     w10;
    double sum1 = 0;
    double sum2 = 0;


    //Consider initial matrix Mat as 4 blocks (b00, b01, b10, b11)
    //blocks initialization
    b00 = (double*)malloc(sizeof(double) * BS * BS);
    b01 = (double*)malloc(sizeof(double) * BS * BS);
    b10 = (double*)malloc(sizeof(double) * BS * BS);
    b11 = (double*)malloc(sizeof(double) * BS * BS);
    for (int i = 0; i < BS; ++i)
    {
        for (int j = 0; j < BS; ++j)
        {
            b00[j + i * BS] = *(*mat + j + i * G_COLS);
            b01[j + i * BS] = *(*mat + BS + j + i * G_COLS);
            b10[j + i * BS] = *(*mat + j + (i + BS) * G_COLS);
            b11[j + i * BS] = *(*mat + BS + j + (i + BS) * G_COLS);
        }
    }
    
    //initialization of utils
    *q = (double*)malloc(sizeof(double) * G_COLS * G_ROWS); //
    v00 = (double*)malloc(sizeof(double) * BS);
    v10 = (double*)malloc(sizeof(double) * BS);
    w00 = (double*)malloc(sizeof(double) * BS);
    w10 = (double*)malloc(sizeof(double) * BS);
    
    //делаем два цикла: по левой половине матрицы и по правой. Так избавляемся от рутины с правым верхним блоком + 
    // объекты становятся легче и охотнее лезут в кэш
    for (int i = 0; i < BS; ++i)
    {
        //задаем вектор v. он состоит из i-го столбца левого верхнего блока b00 и левого нижнего блока v01
        for (int j = 0; j < i; ++j)
            *(v00 + j) = 0;
        
        for (int j = i; j < BS; ++j)
            *(v00 + j) = b00[i + j * BS];

        for (int j = 0; j < BS; ++j)
            *(v10 + j) = b10[i + j * BS];
        

        //норма v до смещения 
        x2 = 0;
        for (int k = 0; k < BS; ++k)
            x2 += v00[k] * v00[k] + v10[k] * v10[k];
        x2 = sqrt(x2);

        //смещение i-й компоненты v
        v00[i] += (double)sign(v00[i]) * x2;

        //сохраняем v в матрицу Q
        for (int k = 0; k < BS; ++k)
            (*q)[i + k * G_COLS] =  v00[k];   
        for (int k = 0; k < BS; ++k)
            (*q)[i + (k + BS) * G_COLS] =  v10[k];          

        //считаем скалярное пр-е v*v. потом на него будем делить
        x1 = 0;
        for (int k = 0; k < BS; ++k)
            x1 += v00[k] * v00[k] + v10[k] * v10[k];
        //w00

        //считаем векторы поправки: w = A^T v. Также делим w напополам, поэтому ниже два цикла, по одному на каждую половинку
        for (int j = 0; j < BS; ++j)
        {
            sum1 = 0;
            sum2 = 0;
            for (int k = 0; k < BS; ++k)
            {
                sum1 += b00[j + k * BS] * v00[k] + b10[j + k * BS] * v10[k];
                sum2 += b01[j + k * BS] * v00[k] + b11[j + k * BS] * v10[k];
            }
            w00[j] = sum1;
            w10[j] = sum2;
        }

        //вычитаем одноранговую поправку из основной матрицы
        for (int j = 0; j < BS; ++j)
        {
            for (int k = 0; k < BS; ++k)
            {   
                b00[k + j * BS] -= 2 * v00[j] * w00[k] / x1;
                b01[k + j * BS] -= 2 * v00[j] * w10[k] / x1;
                b10[k + j * BS] -= 2 * v10[j] * w00[k] / x1;
                b11[k + j * BS] -= 2 * v10[j] * w10[k] / x1;
            }
        }
    }

    //для преобразования второй половинки требуется меньше действий. вместо четырех блоков b.. используем только b11,
    //ведь верхняя половина вектора v  занулится и не будет делать вклад в поправку. по этой причине также используем только
    //одну половину вектора v и одну половину вектора w. в целом все как наверху

    for (int i = 0; i < BS - 1; ++i)
    {
        //set x
        for (int j = 0; j < i; ++j)
            *(v00 + j) = 0;
        
        for (int j = i; j < BS; ++j)
            *(v00 + j) = b11[i + j * BS];
        
        x2 = 0;
        for (int k = 0; k < BS; ++k)
            x2 += v00[k] * v00[k];
        x2 = sqrt(x2);
        //set v
        v00[i] += (double)sign(v00[i]) * x2;
        for (int k = 0; k < BS; ++k)
        {
            *(*q + BS + i + k * G_COLS) = 0;

        }
        for (int k = 0; k < BS; ++k)
        {
            *(*q + BS + i + (k + BS) * G_COLS) = v00[k];   
        }
        x1 = 0;
        for (int k = 0; k < BS; ++k)
            x1 += v00[k] * v00[k];
        //w00
        for (int j = 0; j < BS; ++j)
        {
            sum1 = 0;
            for (int k = 0; k < BS; ++k)
                sum1 += b11[j + k * BS] * v00[k];
            w00[j] = sum1;
        }

        for (int j = 0; j < BS; ++j)
        {
            for (int k = 0; k < BS; ++k)
                b11[k + j * BS] -= 2 * v00[j] * w00[k] / x1;
        }
    }

    //осталось сохранить результат в mat (берем и переносим 4 блока b назад в mat)
    for (int i = 0; i < BS; ++i)
        for (int j = 0; j < BS; ++j)
        {
            *(*mat + j + i * G_COLS) = b00[j + i * BS];
            *(*mat + BS + j + i * G_COLS) = b01[j + i * BS];
            *(*mat + j + (i + BS) * G_COLS) = b10[j + i * BS];
            *(*mat + j + BS + (i + BS) * G_COLS) = b11[j + i * BS];

        }
    return;
}

//данная ф-я реализует проверку ||QR-mat||. возвращаемое значение - искомая норма
double check(const double *mat, const double *q, const double *r)
{
    //вычисляем матрицу Q. Изначально Q = I(N x N)
    double* q_fin, *E,  *P, *q_fin_res;
    double n;
    double sum;
    double norm;



    q_fin = (double*)malloc(sizeof(double) * G_COLS * G_ROWS);
    q_fin_res = (double*)malloc(sizeof(double) * G_COLS * G_ROWS);
    E = (double*)malloc(sizeof(double) * G_COLS * G_ROWS);
    P = (double*)malloc(sizeof(double) * G_COLS * G_ROWS);
    for (int i = 0; i < G_ROWS; ++i)
        for (int j = 0; j < G_COLS; ++j)
            q_fin[j + i * G_COLS] = (i == j) ? 1 : 0;

    for (int i = 0; i < G_ROWS; ++i)
        for (int j = 0; j < G_COLS; ++j)
            E[j + i * G_COLS] = (i == j) ? 1 : 0;
    
    for (int i = 0; i < G_COLS - 1; ++i)
    {
        //считаем норму v(i-й столбец q), и Qij = Qij - 2QvTv/norm(ij)
        n = 0;
        for (int j = i; j < G_ROWS; ++j)
            n += q[i + j * G_COLS] * q[i + j * G_COLS];
        
        //считаем отражение

        for (int j = 0; j < G_ROWS; ++j)
        {
            sum = 0;
            for (int k = 0; k < G_COLS; ++k)
                P[k + j*G_COLS] = E[k + j*G_COLS] - 2 * q[i + j * G_COLS] * q[i + k * G_COLS] / n;
        }

        // Q=QP
        //save q
        for (int j = 0; j < G_COLS * G_ROWS; ++j)
            q_fin_res[j] = q_fin[j];

        for (int j = 0; j < G_ROWS; ++j)
        {
            for (int k = 0; k < G_COLS; ++k)
            {
                sum = 0;
                for (int t = 0; t < G_COLS; ++t)
                    sum += q_fin_res[t + j * G_COLS] * P[k + t * G_COLS];
                q_fin[k + j * G_COLS] = sum;
            }
        }
    }
    //когда применили все отражения - нужно посчитать QR
    norm = 0;
    for (int i = 0; i < G_ROWS; ++i)
    {
        for (int j = 0; j < G_COLS; ++j)
        {
            sum = 0;
            for (int k = 0; k < G_ROWS; ++k)
                sum += q_fin[k + i * G_COLS] * r[j + G_COLS * k];
            norm += sum - mat[j + i * G_COLS];
        }
    }
    //возвращаем полученную норму
    return norm;
}


int main(int argc, char** argv)
{
    double *mat;
    double *mat1;
    double *mat_blas;
    double *mat_lapack;
    double *q;
    int times;

    if (argc && argv)
        {}
        ;

    mat = (double*)malloc(sizeof(double) * G_COLS * G_ROWS);
    //generate matrix

    srand((unsigned int)(time(NULL)));
    for (int i = 0; i < G_ROWS * G_COLS; ++i)
        mat[i] = (double)((double)rand()/(double)(RAND_MAX)) * 1000;

    
    mat[0] = 2;
    mat[1] = 3;
    mat[2] = 3;
    mat[3] = 2;
    // mat->M[0][1] = 4;
    // mat->M[1][0] = 5;
    // mat->M[1][1] = 4;

    //copy matrix to save it for further checks
    mat1 = (double*)malloc(sizeof(double) * G_COLS * G_ROWS);
    for (int i = 0; i < G_ROWS * G_COLS; ++i)
        mat1[i] = mat[i];

    mat_blas = (double*)malloc(sizeof(double) * G_COLS * G_ROWS);
    for (int i = 0; i < G_ROWS * G_COLS; ++i)
        mat_blas[i] = mat[i];

    mat_lapack = (double*)malloc(sizeof(double) * G_COLS * G_ROWS);
    for (int i = 0; i < G_ROWS * G_COLS; ++i)
        mat_lapack[i] = mat[i];

    if (BS <= 3)
    {
        printf("Mat output\n");
        for (int i = 0; i < G_ROWS; ++i)
        {
            for (int j = 0; j < G_COLS; ++j)
                printf("%.6f ", mat[j + i * G_COLS]);
            printf("\n");
        }
    }
    

    // mat_put(mat); // uncomment only for small matrices
    // mat_bkp = mat_cpy(mat);

    printf("now do hh\n");
    //invoke housholder
    struct timeval start, end;

    double time_taken;
    gettimeofday(&start, NULL);
    //hh not blas

    if (BS == 64)
    {
        times = 2048;
        for (int k = 0; k < times; ++k)
            householder(&q, &mat);
    }
    if (BS == 128)
    {
        times = 512;
        for (int k = 0; k < times; ++k)
            householder(&q, &mat);
    }
    if (BS == 256)
    {
        times = 64;
        for (int k = 0; k < times; ++k)
            householder(&q, &mat);
    }
    if (BS == 512)
    {
        times = 8;
        for (int k = 0; k < times; ++k)
            householder(&q, &mat);
    }
    if (BS == 1024)
    {
        times = 1;
        for (int k = 0; k < times; ++k)
            householder(&q, &mat);
    }
    if (BS <= 3)
    {
        times = 1;
        for (int k = 0; k < times; ++k)
            householder(&q, &mat);
        printf("Q output\n");
        for (int i = 0; i < G_ROWS; ++i)
        {
            for (int j = 0; j < G_COLS; ++j)
                printf("%.6f ", q[j + i * G_COLS]);
            printf("\n");
        }
        printf("\nR output\n");
        for (int i = 0; i < G_ROWS; ++i)
        {
            for (int j = 0; j < G_COLS; ++j)
                printf("%.6f ", mat[j + i * G_COLS]);
            printf("\n");
        }
    }

    gettimeofday(&end, NULL);

    time_taken = (end.tv_sec - start.tv_sec) * 1e6;
    time_taken = (time_taken + (end.tv_usec - 
                              start.tv_usec)) * 1e-6;
    printf("one hh took %.6f sec avg\n", time_taken / times);
    printf("overall for %d times took %.6f sec\n", times, time_taken);
    //printf("check out: %.6f", check(mat1, q, mat));


    gettimeofday(&start, NULL);

    //hh blas
    if (BS == 64)
    {
        times = 2048;
        for (int k = 0; k < times; ++k)
            householder_blas(&q, &mat_blas);
    }
    if (BS == 128)
    {
        times = 512;
        for (int k = 0; k < times; ++k)
            householder_blas(&q, &mat_blas);
    }
    if (BS == 256)
    {
        times = 64;
        for (int k = 0; k < times; ++k)
            householder_blas(&q, &mat_blas);
    }
    if (BS == 512)
    {
        times = 8;
        for (int k = 0; k < times; ++k)
            householder_blas(&q, &mat_blas);
    }
    if (BS == 1024)
    {
        times = 1;
        for (int k = 0; k < times; ++k)
            householder_blas(&q, &mat_blas);
    }
    if (BS <= 3)
    {
        times = 1;
        for (int k = 0; k < times; ++k)
            householder_blas(&q, &mat_blas);
        printf("Q output\n");
        for (int i = 0; i < G_ROWS; ++i)
        {
            for (int j = 0; j < G_COLS; ++j)
                printf("%.6f ", q[j + i * G_COLS]);
            printf("\n");
        }
        printf("\nR output\n");
        for (int i = 0; i < G_ROWS; ++i)
        {
            for (int j = 0; j < G_COLS; ++j)
                printf("%.6f ", mat_blas[j + i * G_COLS]);
            printf("\n");
        }
    }
     gettimeofday(&end, NULL);

    time_taken = (end.tv_sec - start.tv_sec) * 1e6;
    time_taken = (time_taken + (end.tv_usec - 
                              start.tv_usec)) * 1e-6;
    printf("BLAS one hh took %.6f sec avg\n", time_taken / times);
    printf("BLAS overall for %d times took %.6f sec\n", times, time_taken);
    //printf("check out: %.6f", check(mat1, q, mat_blas));


    gettimeofday(&start, NULL);
    //lapack
        //hh blas
    if (BS == 64)
    {
        times = 2048;
        for (int k = 0; k < times; ++k)
            householder_lapack(&q, &mat_lapack);
    }
    if (BS == 128)
    {
        times = 512;
        for (int k = 0; k < times; ++k)
            householder_lapack(&q, &mat_lapack);
    }
    if (BS == 256)
    {
        times = 64;
        for (int k = 0; k < times; ++k)
            householder_lapack(&q, &mat_lapack);
    }
    if (BS == 512)
    {
        times = 8;
        for (int k = 0; k < times; ++k)
            householder_lapack(&q, &mat_lapack);
    }
    if (BS == 1024)
    {
        times = 1;
        for (int k = 0; k < times; ++k)
            householder_lapack(&q, &mat_lapack);
    }
    if (BS <= 3)
    {
        times = 1;
        for (int k = 0; k < times; ++k)
            householder_lapack(&q, &mat_lapack);
        printf("Q output\n");
        for (int i = 0; i < G_ROWS; ++i)
        {
            for (int j = 0; j < G_COLS; ++j)
                printf("%.6f ", q[j + i * G_COLS]);
            printf("\n");
        }
        printf("\nR output\n");
        for (int i = 0; i < G_ROWS; ++i)
        {
            for (int j = 0; j < G_COLS; ++j)
                printf("%.6f ", mat_lapack[j + i * G_COLS]);
            printf("\n");
        }
    }


    gettimeofday(&end, NULL);

    time_taken = (end.tv_sec - start.tv_sec) * 1e6;
    time_taken = (time_taken + (end.tv_usec - 
                              start.tv_usec)) * 1e-6;
    printf("LAPACK one hh took %.6f sec avg\n", time_taken / times);
    printf("BLAS overall for %d times took %.6f sec\n", times, time_taken);
    // THIS IS TEST FUNCTION, THAT HAS O(n^4) asymp. at least. uncomment for relatively small matrices

    //printf("check out: %.6f", check(mat1, q, mat_lapack));


    return(1);
}