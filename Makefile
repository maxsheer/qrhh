SRCDIR := src
DISTSRC := $(addprefix $(SRCDIR)/, main.c)
FLAGS := -Wall -Werror -Wextra -Ofast
CC := gcc
INCL := -Iinc
PROG := hh
LFLAGS := -lm -Lsrc   -llapacke -llapack -lcblas -lrefblas -lgfortran -ltmglib

all: clean
	$(CC) $(FLAGS) $(INCL) $(DISTSRC) $(LFLAGS) -o $(PROG)

clean:
	rm -f hh
	
 re: clean all