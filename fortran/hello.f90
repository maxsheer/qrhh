program hhouse
    implicit none
    integer :: i,j,n,k
    real(8) :: norm
    real(8), allocatable, dimension(:,:) :: mat_cpy, mat, q, refl
    real(8), allocatable, dimension(:) ::  v, tmp_refl

    ! рандомим входную матрицу
    
    n = 128
    allocate ( mat(n,n) )
    allocate ( mat_cpy(n,n) )
    allocate ( q(n,n) )
    allocate ( refl(n,n) )
    allocate ( tmp_refl(n) )
    allocate(v(n))

    

    do i = 1, n
        do j = 1, n
            if ( i == j ) then
                mat(i,j) = 2
            else
                mat(i,j) = 1
            end if 
            q(i,j) = 0
        end do
    end do

    mat_cpy = mat
    
    !print *, mat

    ! теперь собственно приступаем к алгоритму
    do i = 1, n-1
        v = mat(:,i)
        v(1:i - 1) = 0
        norm = sqrt(dot_product(v, v)) 
        v(i) = v(i) + norm * dsign(REAL(1, 8), v(i))
        q(:, i) = v
        norm = dot_product(v,v)
        tmp_refl = matmul(transpose(mat), v)
        ! считаем поправку
        do j = 1, n
            do k = 1, n
                refl(j, k) = v(j) * tmp_refl(k)
            end do
        end do
        mat = mat - (2 / norm) * refl
    end do
    !print *, mat
    !print *, q

    !call Check(mat_cpy, q, mat, n)

end program 

subroutine Check(A, Q, R, n)
    implicit none
    real(8), dimension(n,n) :: A, Q, R
    real(8), allocatable, dimension(:,:) :: Q_fin, A_fin, E, P, ptmp
    real(8), allocatable, dimension(:) :: v
    real(8) :: norm
    integer :: i, j, k, n


    allocate( Q_fin(n, n) )
    allocate( A_fin(n, n) )
    allocate( E(n, n) )
    allocate( P(n, n) )
    allocate( ptmp(n, n) )
    allocate( v(n) )
    ! формируем матрицу Q для умножения
    do i = 1, n
        do j = 1, n
            if ( i == j ) then
                Q_fin(i, j) = 1
            else
                Q_fin(i, j) = 0
            end if
        end do
    end do

    do i = 1, n
        do j = 1, n
            if ( i == j ) then
                E(i, j) = 1
            else
                E(i, j) = 0
            end if
        end do
    end do

    do i = 1, n - 1
        v = Q(:, i)
        norm = dot_product(v, v)
        do j = 1, n
            do k = 1, n
                ptmp(j, k) = 2 * v(j) * v(k) / norm
            end do
        end do
        P = E - ptmp
        Q_fin = matmul(Q_fin, P)
    end do
    print *, 'Norm'
    print *, A, Q, R
    print *, A - matmul(Q_fin, R)
    print *, norm2(A - matmul(Q_fin, R))
    
end subroutine Check